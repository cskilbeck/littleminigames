mainApp.factory('editorOptions', ['$rootScope', '$uibModal', 'util',
function($rootScope, $uibModal, util) {
    "use strict";

    var editorOptions = {

        theme: 'Monokai',
        options: {
            enableLiveAutocompletion: true,
            showFoldWidgets: true,
            printMargin: 120,
            showLineNumbers: true,
            highlightActiveLine: true,
            highlightGutterLine: true,
            displayIndentGuides: true,
            showPrintMargin: false
        },

        change: function() {
            var p = Q.defer(),
                oldOptions = angular.copy(editorOptions);
            $uibModal.open({
                animation: true,
                templateUrl: '/static/html/editorOptionsModal.html',
                controller: 'EditorOptionsModalInstanceController',
                backdrop: false,
                resolve: {
                    options: function () {
                        return editorOptions;
                    }
                }
            }).result.then(function (result) {
                angular.extend(editorOptions, result);
                util.save('editorOptions', editorOptions);
                p.resolve(result);
            }, function() {
                angular.copy(oldOptions, editorOptions);
                p.reject();
            });
            return p.promise;
        }

    };

    angular.extend(editorOptions, util.load('editorOptions') || {});
    // console.log("Loaded editor options (theme is " + editorOptions.theme + ")");

    return editorOptions;

}]);
