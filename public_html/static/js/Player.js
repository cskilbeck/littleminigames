mainApp.factory('player', [ '$rootScope', '$uibModal', 'ajax', 'status', 'dialog',
function ($rootScope, $uibModal, ajax, status, dialog) {
    "use strict";

    // ID 0 is used by the fixed player, 1-9999 are for players sprinkled around the place, 10000+ are for modal popup players
    var nextframe = 10000,
        frames = {};

    function postMessage(id, text, data) {
        if(id === undefined) {
            return;
        }
        var payload = JSON.stringify({ message: text, data: data });
        if(!frames[id]) {
            frames[id] = {
                window: null,
                messages: []
            };
        }
        else if(frames[id].window) {
            frames[id].window.postMessage(payload, "*");
        }
        else {
            frames[id].messages.push(payload);
        }
    }

    window.addEventListener('message', function(e) {
        var payload, message, data;
        try {
            // check if it came from a player frame
            payload = JSON.parse(e.data);
            if(payload.id !== undefined) {
                if(!frames[payload.id]) {
                    frames[payload.id] = {
                        window: e.source,
                        messages: []
                    };
                }
                else if(!frames[payload.id].window) {
                    frames[payload.id].window = e.source;
                }
                // 1st message is always frame-loaded: flush any outstanding messages which have queued up
                if(payload.message === 'frame-loaded') {
                    while(frames[data.id].messages.length > 0) {
                        postMessage(frames[data.id].messages.shift());
                    }
                }
                else {
                    $rootScope.$broadcast('frame:' + payload.message + payload.id, payload.data );
                }
            }
        }
        catch(err) {
        }
    });

    var player = {

        postMessage: function(id, msg, data) {
            postMessage(id, msg, data);
        }
    };

    return player;

}]);
