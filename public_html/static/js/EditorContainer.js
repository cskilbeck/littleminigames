/// @directive: editor-container
/// @description: make an HTML element an ACE editor

mainApp.directive('editorContainer',
function() {
    "use strict";

    return {
        restrict: 'A',
        link: function($scope, elem, attrs) {
            $scope.frameID = attrs.editorContainer; // which player this editor is targeting
            $scope.editorElement = $(elem)[0];      // the element is the editor container
        }
    };
});

